Eloqua HTTP APIs
================

Integration module for Eloqua's HTTP based APIs.

Usage
-----

The module provide a high-level API for Eloqua's REST API, the
`EloquaRestApi` class. Instances of the class are created and retrieved with the static `EloquaRestApi::instances()` method. An instance can then be used using a simple
pattern where path is matched chaining properties nad methods of the API object (eg. $api->assets->form('21') to get the resource
at /assets/form/21).

A low level API is also provided by the `eloqua_http_api_request()` function. This function performs an HTTP request on
one of Eloqua's HTTP APIs (SOAP or REST). The function re-use Drupal's `drupal_http_request()` API. Internally,
`EloquaRestApi` uses `eloqua_http_api_request()`.

Configuration
-------------

Usage of this integration module requires the following configuration variables:
- `eloqua_http_api_sitename`: The Eloqua site name (aka. company name).
- `eloqua_http_api_username`: The username of a user allowed to consume Eloqua's HTTP APIs.
- `eloqua_http_api_password`: The password for that user.

Being purely an API module, no UI is provided to manage the value of these variables. Therefor, they mus be configured
using the `$conf` array in the settings file for the site.