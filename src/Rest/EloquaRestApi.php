<?php

namespace Drupal\eloqua_http_api\Rest;

/**
 * Eloqua REST API interface.
 *
 * @code
 * $api = EloquaRestApi::instance();
 * $api->assets->form('21');
 * $api->assets->optionList($id);
 * @endcode
 *
 * @package Drupal\eloqua_http_api\Rest
 */
class EloquaRestApi extends EloquaRestApiPathElement {

  /**
   * Existing instances of the class.
   *
   * @var array
   *   The existing instances of the class, indexed by endpoints.
   */
  protected static $instances = array();

  /**
   * Default endpoint for the API.
   */

  protected $defaultOptions = array(
    'cache' => CACHE_TEMPORARY,
  );

  /**
   * Create a new instance for a given endpoint.
   *
   * @param string $endpoint
   *   An API endpoint, one of
   *     - A external URL (with trailing /): The URL of the endpoint.
   *     - A string matching the following pattern 'rest/(standard|data|bulk)'.
   */
  protected function __construct($endpoint) {
    $this->endpoint = $endpoint;
  }

  /**
   * Retrieve the instance for a given endpoint.
   *
   * @param string $endpoint
   *   An API endpoint, one of
   *     - A external URL (with trailing /): The URL of the endpoint.
   *     - A string matching the following pattern 'rest/(standard|data|bulk)'.
   *
   * @return EloquaRestApi
   *   The API object for $endpoint.
   */
  public static function instance($endpoint = 'rest/standard') {
    if (!isset(EloquaRestApi::$instances[$endpoint])) {
      EloquaRestApi::$instances[$endpoint] = new EloquaRestApi($endpoint);
    }
    return EloquaRestApi::$instances[$endpoint];
  }

  /**
   * Retrieves the Eloqua site ID.
   *
   * @return string|null
   *   The Eloqua site ID , or NULL if a error prevent its retrieval.
   */
  public function siteId() {
    try {
      $response = $this->get('', array(), array('endpoint' => 'https://login.eloqua.com/id', 'cache' => CACHE_TEMPORARY));
      return (string) $response->site->id;
    }
    catch (\Exception $e) {
      return NULL;
    }
  }

  /**
   * Perform a Get request to retrieve one or more entities.
   *
   * @param string $path
   *   The path of the entities to retrieve (eg. assets/email/123 or
   *    assets/forms).
   * @param array $parameters
   *   Request parameters as an key/value association array.
   * @param array $options
   *   (optional) An array that can have one or more of the following elements:
   *   - cache: Set to one of the following values to enable caching of the
   *     results:
   *     - CACHE_PERMANENT: Indicates that the item should never be removed
   *       unless explicitly told to using cache_clear_all() with a cache ID.
   *     - CACHE_TEMPORARY: Indicates that the item should be removed at the
   *       next general cache wipe.
   *     - A Unix timestamp: Indicates that the item should be kept at least
   *       until the given time, after which it behaves like CACHE_TEMPORARY.
   *   - endpoint: Set to one of the following values to set the URL of the
   *     endpoint to send to request to (defaults to "rest/standard"):
   *     - A external URL (with trailing /): The URL of endpoint.
   *     - A string matching the following pattern
   *       '(rest|soap)/(standard|data|bulk|dataTransfer|email|externalAction)'
   *       to use to corresponding REST/SOAP API's endpoint as retrieved from
   *       https://login.eloqua.com/id.
   *
   * @return mixed
   *   The decoded JSON data from the HTTP endpoint.
   *
   * @throws \Exception
   *   If the HTTP request fails.
   */
  public function get($path, $parameters = array(), $options = array()) {
    $options += array('endpoint' => 'rest/standard');
    $response = eloqua_http_api_request('GET', $path, $parameters, $options);
    switch ($response->code) {
      case 200;
        // OK.
        return json_decode($response->data);

      case 400;
        // Bad Request
        // TODO: Parse error description.
        // json_decode($response->data)
        throw new \Exception('Validation error.', $response->code);

      case 304;
        // Not Modified.
      case 401;
        // Unauthorized.
      case 403;
        // Forbidden.
      case 429;
        // Too Many Requests.
      case 500;
        // Internal Server Error.
      case 502;
        // Bad Gateway.
      case 503;
        // Service Unavailable.
      default:
        throw new \Exception(format_string('Unexpected response code @code: !data.', array(
          '@code' => $response->code,
          '!data' => $response->data,
        )), $response->code);
    }
  }

  /**
   * Set the (default) value for a request option.
   *
   * @param string $name
   *   The name of the option.
   * @param string $value
   *   The default value for the option.
   *
   * @see EloquaRestApi::get()
   */
  public function setOption($name, $value) {
    if ($value !== NULL) {
      $this->defaultOptions[$name] = $value;
    }
    else {
      unset($this->defaultOptions[$name]);
    }
  }

}

/**
 * Interface to perform request against a path on Eloqua REST API interface.
 *
 * @package Drupal\eloqua_http_api\Rest
 */
class EloquaRestApiPathElement {

  protected $name;
  protected $parent;
  protected $children = array();

  /**
   * Create a new EloquaRestApiPathElement.
   *
   * @param string $name
   *   The name of the element.
   * @param EloquaRestApiPathElement $parent
   *   The parent element.
   */
  protected function __construct($name, EloquaRestApiPathElement $parent) {
    if (!is_string($name)) {
      throw new \InvalidArgumentException('The name of an API path element must be a string.');
    }
    $this->name = $name;
    $this->parent = $parent;
  }

  /**
   * Return the children of this path element.
   */
  public function __get($name) {
    if (!isset($this->children[$name])) {
      $this->children[$name] = new EloquaRestApiPathElement($name, $this);
    }
    return $this->children[$name];
  }

  /**
   * Get this path element.
   */
  public function __call($name, $arguments) {
    $arguments += array(
      0 => array(),
      1 => array(),
      2 => array(),
    );
    if (is_array($arguments[0])) {
      return $this->__get($name)->get('', $arguments[0], $arguments[1]);
    }
    else {
      return $this->__get($name)->get($arguments[0], $arguments[1], $arguments[2]);
    }
  }

  /**
   * Perform a GET request on the path element.
   *
   * @param string $subpath
   *   The subpath of the entities to retrieve (eg. '123' or 'forms').
   * @param array $parameters
   *   Request parameters as an key/value association array.
   * @param array $options
   *   (optional) An array that can have one or more of the following elements:
   *   - cache: Set to one of the following values to enable caching of the
   *     results:
   *     - CACHE_PERMANENT: Indicates that the item should never be removed
   *       unless explicitly told to using cache_clear_all() with a cache ID.
   *     - CACHE_TEMPORARY: Indicates that the item should be removed at the
   *       next general cache wipe.
   *     - A Unix timestamp: Indicates that the item should be kept at least
   *       until the given time, after which it behaves like CACHE_TEMPORARY.
   *
   * @return mixed
   *   The decoded JSON data from the HTTP endpoint.
   *
   * @throws \Exception
   *   If the HTTP request fails.
   */
  public function get($subpath, $parameters = array(), $options = array()) {
    unset($options['endpoint']);
    return $this->parent->get("{$this->name}/{$subpath}", $parameters, $options);
  }

}
